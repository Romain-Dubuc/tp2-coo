import { Router } from "express";
import { createOne, deleteOne, getAll, getOne } from "../controllers/items.controller";

const router = Router({ mergeParams: true });

// Affiche l'ensemble des items
router.get("/", async (_req, res) => getAll(res))

// Crée un item
router.post("/", async (req, res) => createOne(req, res))

// Affiche un item avec un id donné
router.get("/:id", async (req, res) => getOne(req, res));

// Supprime un item avec un id donné
router.delete("/:id", async (req, res) => deleteOne(req, res))

export default router
