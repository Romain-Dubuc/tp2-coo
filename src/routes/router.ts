import { Router } from "express";
import charactersRoutes from "./characters.routes";
import itemsRoutes from "./items.routes";
import usersRoutes from "./users.routes";

export function createRouter() {
    const router = Router();

    // L'ensemble des routes pour les characters/items/utilisateurs
    router.use('/:version/characters', charactersRoutes)
    router.use('/:version/items', itemsRoutes)
    router.use('/:version/users', usersRoutes)

    return router;
}
