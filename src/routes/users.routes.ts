import { Router } from "express";
import { getAll, getCharacters, getOne } from "../controllers/users.controller";

const router = Router({ mergeParams: true });

// Affiche l'ensemble des utilisateurs
router.get("/", async (req, res) => getAll(req, res));

// Affiche un utilisateur avec un id donné
router.get("/:id", async (req, res) => getOne(req, res));

// Affiche l'ensemble des characters d'un utilisateur avec un id donné
router.get("/:id/characters", async (req, res) => getCharacters(req, res));

export default router
