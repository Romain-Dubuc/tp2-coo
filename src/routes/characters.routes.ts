import { Router } from "express";
import { updateOne, getAll, getItems, getOne, putItem, removeItem } from "../controllers/characters.controller";

const router = Router({ mergeParams: true });

// Affiche l'ensemble des characters
router.get("/", async (_req, res) => getAll(res));

// Affiche un character avec un id donné
router.get("/:id", async (req, res) => getOne(req, res));

// Modifie un character
router.put("/:id", async (req, res) => updateOne(req, res))

// Affiche l'ensemble des items d'un character donné
router.get("/:id/items", async (req, res) => getItems(req, res))

// Ajoute un item donné à un utilisateur donné
router.post("/:id_character/items/:id_item", async (req, res) => putItem(req, res))

// Retire un item donné à un utilisateur donné
router.delete("/:id_character/items/:id_item", async (req, res) => removeItem(req, res))

export default router
