import { Response, Request } from "express"
import Item from "../classes/Item"
import { checkItemFields } from "../middlewares/fields.middleware"
import { createItem, deleteItem, getItemById, getItems } from "../models/items.models"

export async function getAll(response: Response): Promise<void> {
    const items = await getItems()
    response.json({ items })
}

export async function getOne(request: Request, response: Response): Promise<void> {
    const { id } = request.params;
    const item = await getItemById(id)

    if (!item) {
        response.sendStatus(404);
    } else {
        response.json({ item });
    }
}

export async function createOne(request: Request, response: Response): Promise<void> {
    const fields = request.body
    if ('name' in fields && 'price' in fields && 'type' in fields) {
        let item_object = new Item(
            '',
            fields.name,
            fields.type,
            fields.price,
            fields.attributes ?? []
        )

        if (checkItemFields(item_object)) {
            const new_item = await createItem(item_object)

            response.json({ item: new_item });
        } else {
            response.sendStatus(400);
        }
    } else {
        response.sendStatus(400);
    }
}

export async function deleteOne(request: Request, response: Response): Promise<void> {
    const { id } = request.params;
    const item = await getItemById(id)

    if (!item) {
        response.sendStatus(404);
    } else {
        const items = await deleteItem(item)
        response.json({ items });
    }
}
