import { Response, Request } from "express"
import { getCharactersByUser } from "../models/characters.models"
import { getUserByIdAndVersion, getUsersByVersion } from "../models/users.models"

export async function getAll(request: Request, response: Response): Promise<void> {
    const { version } = request.params
    const users = await getUsersByVersion(version)
    response.json({ users })
}

export async function getOne(request: Request, response: Response): Promise<void> {
    const { id, version } = request.params;
    const user = await getUserByIdAndVersion(id, version)

    if (!user) {
        response.sendStatus(404);
    } else {
        response.json({ user });
    }
}

export async function getCharacters(request: Request, response: Response): Promise<void> {
    const { id, version } = request.params;
    const user = await getUserByIdAndVersion(id, version)

    if (!user) {
        response.sendStatus(404);
    } else {
        const userCharacters = await getCharactersByUser(user)
        response.json({ characters: userCharacters });
    }
}
