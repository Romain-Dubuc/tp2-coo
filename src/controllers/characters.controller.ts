import { Request, Response } from "express";
import Character from "../classes/Character";
import { checkCharacterFields } from "../middlewares/fields.middleware";
import { getCharacterById, getCharacters, putItemInCharacter, removeItemInCharacter, updateCharacter } from "../models/characters.models";
import { getItemById, getItemsByCharacter } from "../models/items.models";

export async function getAll(response: Response): Promise<void> {
    const characters = await getCharacters()
    response.json({ characters });
}

export async function getOne(request: Request, response: Response): Promise<void> {
    const { id } = request.params;
    const character = await getCharacterById(id)

    if (!character) {
        response.sendStatus(404);
    } else {
        response.json({ character });
    }
}

export async function updateOne(request: Request, response: Response): Promise<void> {
    const { id } = request.params;
    const character = await getCharacterById(id)

    if (!character) {
        response.sendStatus(404)
    } else {
        let character_object = new Character(
            parseInt(id),
            request.body.name ?? character.getName(),
            request.body.goldAmount ?? character.getGoldAmount(),
            request.body.health ?? character.getHealth(),
            request.body.manaLevel ?? character.getManaLevel(),
            request.body.forceLevel ?? character.getForceLevel(),
            request.body.coordinates ?? character.getCoordinates(),
            character.getItems(),
            request.body.weight ?? character.getWeight(),
            request.body.height ?? character.getHeight()
        )

        if (checkCharacterFields(character_object)) {
            const new_character = await updateCharacter(character_object)

            response.json({ character: new_character })
        } else {
            response.sendStatus(400)
        }
    }
}

export async function getItems(request: Request, response: Response): Promise<void> {
    const { id } = request.params;
    const character = await getCharacterById(id)

    if (!character) {
        response.sendStatus(404);
    } else {
        const characterItems = await getItemsByCharacter(character)
        response.json({ characterItems });
    }
}

export async function putItem(request: Request, response: Response): Promise<void> {
    const { id_character, id_item } = request.params;
    const character = await getCharacterById(id_character)
    const item = await getItemById(id_item)

    if (!character || !item) {
        response.sendStatus(404);
    } else {
        const character_updated = await putItemInCharacter(character, item)
        response.json({ character: character_updated });
    }
}

export async function removeItem(request: Request, response: Response): Promise<void> {
    const { id_character, id_item } = request.params;
    const character = await getCharacterById(id_character)

    if (!character) {
        response.sendStatus(404);
    } else {
        const character_updated = await removeItemInCharacter(character, id_item)
        response.json({ character: character_updated });
    }
}
