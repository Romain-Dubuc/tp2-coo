import { HttpServer } from "./Server";
import { App } from "./App";

import { createRouter } from "./routes/router";

const server = new HttpServer(3000, new App(createRouter()).instance);
server.start();

// Inject data to local storage
import "./data";
