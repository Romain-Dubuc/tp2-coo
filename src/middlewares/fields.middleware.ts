import Character from "../classes/Character"
import Item from "../classes/Item"
import { Attribute } from "../interfaces/IAttributes"

export function checkCharacterFields(character_object: Character): boolean {
    if (typeof character_object.getName() !== 'string') {
        return false
    }

    if (
        typeof character_object.getGoldAmount() !== 'number' ||
        typeof character_object.getHealth() !== 'number' ||
        typeof character_object.getManaLevel() !== 'number' ||
        typeof character_object.getForceLevel() !== 'number'
    ) {
        return false
    }

    const coordinates = character_object.getCoordinates()

    if (
        Object.keys(coordinates).length !== 3 ||
        !('x' in coordinates) ||
        !('y' in coordinates) ||
        !('z' in coordinates)
    ) {
        return false
    }

    if (
        typeof coordinates.x !== 'number' ||
        typeof coordinates.y !== 'number' ||
        typeof coordinates.z !== 'number'
    ) {
        return false
    }

    const weight = character_object.getWeight()

    if (
        Object.keys(weight).length !== 2 ||
        !('unit' in weight) ||
        !('value' in weight)
    ) {
        return false
    }

    if (
        typeof weight.unit !== 'string' ||
        typeof weight.value !== 'number'
    ) {
        return false
    }

    const height = character_object.getHeight()

    if (
        Object.keys(height).length !== 2 ||
        !('unit' in height) ||
        !('value' in height)
    ) {
        return false
    }

    if (
        typeof height.unit !== 'string' ||
        typeof height.value !== 'number'
    ) {
        return false
    }

    return true
}

export function checkItemFields(item_object: Item): boolean {
    if (
        typeof item_object.getName() !== 'string' ||
        typeof item_object.getType() !== 'string'
    ) {
        return false
    }

    if (typeof item_object.getPrice() !== 'number') {
        return false
    }

    if (item_object.getAttributes()) {
        const attributes = item_object.getAttributes()
        if (!Array.isArray(attributes)) {
            return false
        }

        return attributes.every((attribute: Attribute) => {
            if (
                Object.keys(attribute).length !== 2 ||
                !('type' in attribute) ||
                !('value' in attribute)
            ) {
                return false
            }

            if (
                typeof attribute.type !== 'string' ||
                typeof attribute.value !== 'string'
            ) {
                return false
            }
            return true
        });
    }

    return true
}
