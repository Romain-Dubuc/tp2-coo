import { PaymentType } from './PaymentType'

export default class PaymentMean {
    #id: string
    #type: PaymentType
    #informations: Record<string, unknown>

    constructor(id: string, type: PaymentType, informations: Record<string, unknown>) {
        this.#id = id
        this.#type = type
        this.#informations = informations
    }
}
