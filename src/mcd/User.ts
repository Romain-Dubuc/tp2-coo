import PaymentMean from './PaymentMean'
import Purchase from './Purchase'

export default class User {
    #wallet: PaymentMean[]
    #purchases: Purchase[]

    constructor(wallet: PaymentMean[], purchases: Purchase[]) {
        this.#wallet = wallet
        this.#purchases = purchases
    }
}
