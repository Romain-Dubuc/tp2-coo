export default class Purchase {
    readonly #id: string
    readonly #amount: number
    readonly #date: Date

    constructor(id: string, amount: number, date: Date) {
        this.#id = id
        this.#amount = amount
        this.#date = date
    }
}
