export enum PaymentType {
    PAYPAL,
    CREDIT_CARD,
    GIFTCARD
}
