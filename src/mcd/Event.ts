import Card from './Card'

export default class Event implements Card {
    readonly id: string
    url: string
    title: string
    description: string
    image: string

    constructor(id: string, url: string, title: string, description: string, image: string) {
        this.id = id
        this.url = url
        this.title = title
        this.description = description
        this.image = image
    }

    register(userId: string): boolean {
        return true
    }
}
