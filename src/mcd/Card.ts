export default interface Card {
    readonly id: string
    url: string
    title: string
    description: string
    image: string
}
