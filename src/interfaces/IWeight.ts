export interface Weight {
    unit: string
    value: number
}
