export interface Attribute {
    type: string
    value: string
}
