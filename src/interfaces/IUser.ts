export interface IUser {
    id: string
    oldId: number
    username: string
    playedCharacter: number
    characters: number[]
    getId(): string
    getCharacters(): number[]
}
