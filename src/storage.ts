import storage from "node-persist";
import Character from "./classes/Character";
import { db } from "./db";
import Item from "./classes/Item";
import User from "./classes/UserV1";
import UserV2 from "./classes/UserV2";
import { IUser } from "./interfaces/IUser";
import UserAdapter from "./classes/UserAdapter";

export async function initStorage() {
  await storage.init({
    dir: "./data/node-persist",
    stringify: JSON.stringify,
    parse: JSON.parse,
    encoding: "utf8",
    logging: true,
  });
}

export async function getStorageCharacters(): Promise<Character[]> {
  let characters = []
  const characters_storage = await storage.getItem("characters")
  for (let character_storage of characters_storage) {
    characters.push(new Character(
      character_storage.id,
      character_storage.name,
      character_storage.goldAmount,
      character_storage.health,
      character_storage.manaLevel,
      character_storage.forceLevel,
      character_storage.coordinates,
      character_storage.items,
      character_storage.weight,
      character_storage.height)
    )
  }

  return characters
}

export async function getStorageUsers(version: string): Promise<IUser[]> {
  let users = []

  // Factory for making users
  if (version === "v1") {
    const users_storage = await storage.getItem("users")
    for (let user_storage of users_storage) {
      users.push(new UserAdapter(new User(
        user_storage.id,
        user_storage.name,
        user_storage.selectedCharacter,
        user_storage.characters)
      ))
    }
  } else if (version === "v2") {
    const users_storage = db.get("users").value()
    for (let user_storage of users_storage) {
      users.push(new UserV2(
        user_storage.id,
        user_storage.old_id,
        user_storage.username,
        user_storage.playedCharacter,
        user_storage.characters)
      )
    }
  }

  return users
}

export async function getStorageItems(): Promise<Item[]> {
  let items = []
  const items_storage = await storage.getItem("items")
  for (let item_storage of items_storage) {
    items.push(new Item(
      item_storage.id,
      item_storage.name,
      item_storage.type,
      item_storage.price,
      item_storage.attributes)
    )
  }

  return items
}
