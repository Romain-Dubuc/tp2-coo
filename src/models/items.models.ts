import Character from "../classes/Character"
import storage from "node-persist";
import Item from "../classes/Item"
import { getStorageItems, initStorage } from "../storage"

export async function getItems(): Promise<Item[]> {
    await initStorage()
    return await getStorageItems()
}

export async function getItemsByCharacter(character: Character): Promise<(Item | undefined)[]> {
    await initStorage()
    const items = await getStorageItems()
    const character_items = character.getItems().map((id_item: string) => {
        return items.find(
            (item: Item) => item.getId() === id_item
        )
    })
    return character_items
}

export async function createItem(item: Item): Promise<Item> {
    await initStorage()
    let items = await getStorageItems()
    item.setId('item_' + (items.length + 1))
    items.push(item)

    await storage.setItem("items", items)
    return item
}

export async function getItemById(id_item: string): Promise<Item | undefined> {
    await initStorage()
    const items = await getStorageItems()

    return items.find(
        (item: Item) => item.getId() === id_item
    )
}

export async function deleteItem(item_deleted: Item): Promise<Item[]> {
    await initStorage()
    let items = await getStorageItems()
    items = items.filter((item: Item) => item.getId() !== item_deleted.getId())

    await storage.setItem("items", items)
    return items
}
