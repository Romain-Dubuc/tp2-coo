import { IUser } from "../interfaces/IUser"
import { getStorageUsers, initStorage } from "../storage"

export async function getUsersByVersion(version: string): Promise<IUser[] | undefined> {
    await initStorage()
    let users = await getStorageUsers(version)

    return users
}

export async function getUserByIdAndVersion(id: string, version: string): Promise<IUser | undefined> {
    await initStorage()
    const users = await getUsersByVersion(version)

    if (users) {
        return users.find((user: IUser) => user.getId() == id)
    } else return undefined
}
