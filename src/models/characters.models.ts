import storage from "node-persist";
import Character from "../classes/Character";
import Item from "../classes/Item";
import { IUser } from "../interfaces/IUser";
import { getStorageCharacters, initStorage } from "../storage";

export async function getCharacters(): Promise<Character[]> {
    await initStorage();
    return await getStorageCharacters();
}

export async function getCharacterById(id_character: string): Promise<Character | undefined> {
    await initStorage()
    const characters = await getStorageCharacters();

    return characters.find(
        (character: Character) => String(character.getId()) === id_character
    );
}

export async function updateCharacter(character_updated: Character): Promise<Character> {
    await initStorage()

    let characters = await getStorageCharacters();
    const character_index = characters.findIndex((character: Character) => character.getId() === character_updated.getId())
    characters[character_index] = character_updated
    await storage.setItem("characters", characters)

    return character_updated;
}

export async function putItemInCharacter(character_updated: Character, item: Item): Promise<Character> {
    await initStorage()
    let characters = await getStorageCharacters()
    const character_index = characters.findIndex((character: Character) => character.getId() === character_updated.getId())
    characters[character_index].getItems().push(item.getId())
    await storage.setItem("characters", characters)

    return characters[character_index]
}

export async function removeItemInCharacter(character_updated: Character, id_item: string): Promise<Character> {
    await initStorage()
    let characters = await getStorageCharacters()
    const character_index = characters.findIndex((character: Character) => character.getId() === character_updated.getId())
    let characters_items = characters[character_index].getItems()
    const item_index = characters_items.findIndex((item: string) => item === id_item)

    if (item_index > -1) {
        characters_items.splice(item_index, 1)
    }

    await storage.setItem("characters", characters)
    return characters[character_index]
}

export async function getCharactersByUser(user: IUser): Promise<Character[]> {
    await initStorage()
    const characters = await getStorageCharacters();
    return characters.filter((character: Character) =>
        user.getCharacters().find((id_user_character: string | number) => id_user_character == character.getId())
    );
}
