export default class User {
    id: number
    name: string
    selectedCharacter: string
    characters: string[]

    constructor(id: number, name: string, selectedCharacter: string, characters: string[]) {
        this.id = id
        this.name = name
        this.selectedCharacter = selectedCharacter
        this.characters = characters
    }

    public getId(): number {
        return this.id;
    }

    public setId(id: number): void {
        this.id = id;
    }

    public getName(): string {
        return this.name;
    }

    public setName(name: string): void {
        this.name = name;
    }

    public getSelectedCharacter(): string {
        return this.selectedCharacter;
    }

    public setSelectedCharacter(selectedCharacter: string): void {
        this.selectedCharacter = selectedCharacter;
    }

    public getCharacters(): string[] {
        return this.characters;
    }

    public setCharacters(characters: string[]): void {
        this.characters = characters;
    }
}
