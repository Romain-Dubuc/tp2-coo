import { Coordinates } from "../interfaces/ICoordinates"
import { Height } from "../interfaces/IHeight"
import { Weight } from "../interfaces/IWeight"

export default class Entity {

    private id: number
    private name: string
    private goldAmount: number
    private health: number
    private manaLevel: number
    private forceLevel: number
    private coordinates: Coordinates
    private items: string[]
    private weight: Weight
    private height: Height


    public getId(): number {
        return this.id;
    }

    public setId(id: number): void {
        this.id = id;
    }

    public getName(): string {
        return this.name;
    }

    public setName(name: string): void {
        this.name = name;
    }

    public getGoldAmount(): number {
        return this.goldAmount;
    }

    public setGoldAmount(goldAmount: number): void {
        this.goldAmount = goldAmount;
    }

    public getHealth(): number {
        return this.health;
    }

    public setHealth(health: number): void {
        this.health = health;
    }

    public getManaLevel(): number {
        return this.manaLevel;
    }

    public setManaLevel(manaLevel: number): void {
        this.manaLevel = manaLevel;
    }

    public getForceLevel(): number {
        return this.forceLevel;
    }

    public setForceLevel(forceLevel: number): void {
        this.forceLevel = forceLevel;
    }

    public getCoordinates(): Coordinates {
        return this.coordinates;
    }

    public setCoordinates(coordinates: Coordinates): void {
        this.coordinates = coordinates;
    }

    public getItems(): string[] {
        return this.items;
    }

    public setItems(items: string[]): void {
        this.items = items;
    }

    public getWeight(): Weight {
        return this.weight;
    }

    public setWeight(weight: Weight): void {
        this.weight = weight;
    }

    public getHeight(): Height {
        return this.height;
    }

    public setHeight(height: Height): void {
        this.height = height;
    }



    constructor(id: number, name: string, goldAmount: number, health: number, manaLevel: number,
        forceLevel: number, coordinates: Coordinates, items: string[], weight: Weight, height: Height) {
        this.id = id
        this.name = name
        this.goldAmount = goldAmount
        this.health = health
        this.manaLevel = manaLevel
        this.forceLevel = forceLevel
        this.coordinates = coordinates
        this.items = items
        this.weight = weight
        this.height = height
    }

}
