import { Attribute } from "../interfaces/IAttributes"

export default class Item {
    private id: string
    private name: string
    private type: string
    private price: number
    private attributes: Attribute[]

    constructor(id: string, name: string, type: string, price: number, attributes: Attribute[]) {
        this.id = id
        this.name = name
        this.type = type
        this.price = price
        this.attributes = attributes ?? []
    }

    public getId(): string {
        return this.id;
    }

    public setId(id: string): void {
        this.id = id;
    }

    public getName(): string {
        return this.name;
    }

    public setName(name: string): void {
        this.name = name;
    }

    public getType(): string {
        return this.type;
    }

    public setType(type: string): void {
        this.type = type;
    }

    public getPrice(): number {
        return this.price;
    }

    public setPrice(price: number): void {
        this.price = price;
    }

    public getAttributes(): Attribute[] {
        return this.attributes;
    }

    public setAttributes(attributes: Attribute[]): void {
        this.attributes = attributes;
    }
}
