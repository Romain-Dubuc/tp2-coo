import { Coordinates } from "../interfaces/ICoordinates";
import { Height } from "../interfaces/IHeight";
import { Weight } from "../interfaces/IWeight";
import Entity from "./Entity";

export default class Mob extends Entity {

    constructor(id: number, name: string, goldAmount: number, health: number, manaLevel: number,
        forceLevel: number, coordinates: Coordinates, items: string[], weight: Weight, height: Height) {
        super(id, name, goldAmount, health, manaLevel, forceLevel, coordinates, items, weight, height)
    }

}
