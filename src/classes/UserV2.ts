import { IUser } from "../interfaces/IUser"

export default class UserV2 implements IUser {
    id: string
    oldId: number
    username: string
    playedCharacter: number
    characters: number[]

    constructor(id: string, oldId: number, username: string, playedCharacter: number, characters: number[]) {
        this.id = id
        this.oldId = oldId
        this.username = username
        this.playedCharacter = playedCharacter
        this.characters = characters
    }

    public getId(): string {
        return this.id;
    }

    public setId(id: string): void {
        this.id = id;
    }

    public getOldId(): number {
        return this.oldId;
    }

    public setOldId(oldId: number): void {
        this.oldId = oldId;
    }

    public getUsername(): string {
        return this.username;
    }

    public setUsername(username: string): void {
        this.username = username;
    }

    public getPlayedCharacter(): number {
        return this.playedCharacter;
    }

    public setPlayedCharacter(playedCharacter: number): void {
        this.playedCharacter = playedCharacter;
    }

    public getCharacters(): number[] {
        return this.characters;
    }

    public setCharacters(characters: number[]): void {
        this.characters = characters;
    }
}
