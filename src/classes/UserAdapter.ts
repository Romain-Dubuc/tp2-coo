import { IUser } from "../interfaces/IUser";
import User from "./UserV1";

export default class UserAdapter implements IUser {
    id: string
    oldId: number
    username: string
    playedCharacter: number
    characters: number[];

    constructor(user: User) {
        this.id = user.getId().toString()
        this.oldId = user.getId()
        this.username = user.getName()
        this.playedCharacter = parseInt(user.getSelectedCharacter())
        this.characters = user.getCharacters().map(Number)
    }

    public getId(): string {
        return this.id;
    }

    public setId(id: string): void {
        this.id = id;
    }

    public getOldId(): number {
        return this.oldId;
    }

    public setOldId(oldId: number): void {
        this.oldId = oldId;
    }

    public getUsername(): string {
        return this.username;
    }

    public setUsername(username: string): void {
        this.username = username;
    }

    public getPlayedCharacter(): number {
        return this.playedCharacter;
    }

    public setPlayedCharacter(playedCharacter: number): void {
        this.playedCharacter = playedCharacter;
    }

    public getCharacters(): number[] {
        return this.characters;
    }

    public setCharacters(characters: number[]): void {
        this.characters = characters;
    }
}
